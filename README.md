# alpine-builds-exporter

## Description

Starting `main.py` starts a HTTP server on Port 8000 which is compatible with
the Prometheus metrics server.

The input is retrieved by WebSocket of the `https://build.alpinelinux.org`
Server, and therefore always up-to-date.

## Usage

To retrieve the latest source, use:

```sh
$ git clone https://gitlab.alpinelinux.org/fossdd/alpine-builds-exporter.git
$ cd alpine-builds-exporter
```

> [!TIP]
> To choose a stable release, checkout the latest tag, e. g.: `git checkout 1.0.0`

### Container

This repository includes a simple Containerfile as a starting point for a
Container.

You may want to start the exporter as container with podman, e. g.:

```sh
$ podman build . -t alpine-builds-exporter
$ podman run -d -p 8000:8000 alpine-builds-exporter
```

### Development

For development purposes, you may want to immediatley start the exporter:
```sh
$ python3 main.py
```

The `alpine.py`-file includes all relevant Classes to retrieve the required
information like Buildserver and Messages.

`main.py` starts the Prometheus server and starts feeding the regristry on a new
message.

## Contributing

Please format the code using `black` and test the code with `flake8`. You may
want to setup [`pre-commit`](https://pre-commit.com/) (`pre-commit install`) to
automatically check before commiting.

To submit the patch, please [open a Merge Request on GitLab](https://gitlab.alpinelinux.org/fossdd/alpine-builds-exporter/-/merge_requests/new)
or alternativly submit the patch to [`~fossdd/public-inbox@lists.sr.ht`](mailto:~fossdd/public-inbox@lists.sr.ht).


## Example output

With this exporter metrics can be shouwn, for example the packages left in the builders' queue.

The following picture shows the builder queue on the python 3.12 rebuild on edge (`alpinelinux_builds_total_progress_total - alpinelinux_builds_total_progress_current`):

![Chart](https://i.supa.codes/owzBE)

(drops are probably caused by websocket resets. Patches welcome!)

### Raw output

```sh
$ curl http://localhost:8000/metrics
# HELP python_gc_objects_collected_total Objects collected during gc
# TYPE python_gc_objects_collected_total counter
python_gc_objects_collected_total{generation="0"} 647.0
python_gc_objects_collected_total{generation="1"} 5.0
python_gc_objects_collected_total{generation="2"} 0.0
# HELP python_gc_objects_uncollectable_total Uncollectable objects found during GC
# TYPE python_gc_objects_uncollectable_total counter
python_gc_objects_uncollectable_total{generation="0"} 0.0
python_gc_objects_uncollectable_total{generation="1"} 0.0
python_gc_objects_uncollectable_total{generation="2"} 0.0
# HELP python_gc_collections_total Number of times this generation was collected
# TYPE python_gc_collections_total counter
python_gc_collections_total{generation="0"} 66.0
python_gc_collections_total{generation="1"} 5.0
python_gc_collections_total{generation="2"} 0.0
# HELP python_info Python platform information
# TYPE python_info gauge
python_info{implementation="CPython",major="3",minor="11",patchlevel="8",version="3.11.8"} 1.0
# HELP process_virtual_memory_bytes Virtual memory size in bytes.
# TYPE process_virtual_memory_bytes gauge
process_virtual_memory_bytes 2.6585088e+08
# HELP process_resident_memory_bytes Resident memory size in bytes.
# TYPE process_resident_memory_bytes gauge
process_resident_memory_bytes 1.5413248e+07
# HELP process_start_time_seconds Start time of the process since unix epoch in seconds.
# TYPE process_start_time_seconds gauge
process_start_time_seconds 1.71224107681e+09
# HELP process_cpu_seconds_total Total user and system CPU time spent in seconds.
# TYPE process_cpu_seconds_total counter
process_cpu_seconds_total 2.6599999999999997
# HELP process_open_fds Number of open file descriptors.
# TYPE process_open_fds gauge
process_open_fds 7.0
# HELP process_max_fds Maximum number of open file descriptors.
# TYPE process_max_fds gauge
process_max_fds 1024.0
# HELP alpinelinux_builds_status_info
# TYPE alpinelinux_builds_status_info gauge
alpinelinux_builds_status_info{builder="build-3-13-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-aarch64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-aarch64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-14-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-armhf",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-aarch64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-15-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-armv7",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-15-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-15-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-14-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-14-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-13-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-x86_64",text="3/7 1548/1553 main/linux-lts 6.6.24-r0"} 1.0
alpinelinux_builds_status_info{builder="build-edge-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-x86_64",text="1/1 6096/6097 community/chromium 123.0.6312.105-r0"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-armhf",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-armv7",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-armhf",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-aarch64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-armv7",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-17-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-15-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-13-x86_64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-aarch64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-armhf",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-riscv64",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-13-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-14-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-edge-armhf",text="failed"} 1.0
alpinelinux_builds_status_info{builder="build-edge-s390x",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-ppc64le",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-16-armv7",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-19-x86",text="idle"} 1.0
alpinelinux_builds_status_info{builder="build-3-18-armv7",text="idle"} 1.0
# HELP alpinelinux_builds_status_type
# TYPE alpinelinux_builds_status_type gauge
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-13-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-13-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-13-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-13-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-aarch64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-aarch64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-14-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-14-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-14-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-14-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-armhf"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-aarch64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-15-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-armv7"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-15-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-15-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-15-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-15-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-15-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-14-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-14-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-13-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-13-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-13-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-13-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-armhf"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-armv7"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-armhf"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-aarch64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-armv7"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-17-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-17-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-17-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-17-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-15-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-15-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-15-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-15-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-13-x86_64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-aarch64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-aarch64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-armhf"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-riscv64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-riscv64"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-riscv64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-riscv64"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-13-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-14-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-14-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-14-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-14-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-armhf"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-armhf"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-edge-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-edge-s390x"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-edge-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-edge-s390x"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-ppc64le"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-16-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-16-armv7"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-16-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-16-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-19-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-19-x86"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-19-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-19-x86"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="MISC",builder="build-3-18-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="IDLE",builder="build-3-18-armv7"} 1.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="PROGRESS",builder="build-3-18-armv7"} 0.0
alpinelinux_builds_status_type{alpinelinux_builds_status_type="ERROR",builder="build-3-18-armv7"} 0.0
# HELP alpinelinux_builds_package_info
# TYPE alpinelinux_builds_package_info gauge
alpinelinux_builds_package_info{builder="build-3-13-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-aarch64"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-aarch64"} 1.0
alpinelinux_builds_package_info{builder="build-3-14-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-armhf"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-aarch64"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-15-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-edge-armv7"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-15-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-3-15-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-3-14-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-14-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-3-13-x86"} 1.0
alpinelinux_builds_package_info{builder="build-edge-x86_64",name="main/linux-lts",version="6.6.24-r0"} 1.0
alpinelinux_builds_package_info{builder="build-edge-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-x86_64",name="community/chromium",version="123.0.6312.105-r0"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-armhf"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-armv7"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-armhf"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-aarch64"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-armv7"} 1.0
alpinelinux_builds_package_info{builder="build-3-17-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-15-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-13-x86_64"} 1.0
alpinelinux_builds_package_info{builder="build-edge-aarch64"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-armhf"} 1.0
alpinelinux_builds_package_info{builder="build-edge-riscv64"} 1.0
alpinelinux_builds_package_info{builder="build-edge-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-13-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-14-x86"} 1.0
alpinelinux_builds_package_info{builder="build-edge-armhf",name="community/nushell",version="0.92.0-r0"} 1.0
alpinelinux_builds_package_info{builder="build-edge-s390x"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-ppc64le"} 1.0
alpinelinux_builds_package_info{builder="build-3-16-armv7"} 1.0
alpinelinux_builds_package_info{builder="build-3-19-x86"} 1.0
alpinelinux_builds_package_info{builder="build-3-18-armv7"} 1.0
# HELP alpinelinux_builds_build_progress_current
# TYPE alpinelinux_builds_build_progress_current gauge
alpinelinux_builds_build_progress_current{builder="build-3-13-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-14-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-19-armhf"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-18-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-edge-armv7"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-19-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-15-s390x"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-18-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-13-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-edge-x86_64"} 3.0
alpinelinux_builds_build_progress_current{builder="build-edge-x86"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-19-x86_64"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-armhf"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-armv7"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-armhf"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-19-armv7"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-17-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-15-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_build_progress_current{builder="build-edge-aarch64"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-18-armhf"} 0.0
alpinelinux_builds_build_progress_current{builder="build-edge-riscv64"} 1.0
alpinelinux_builds_build_progress_current{builder="build-edge-ppc64le"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-16-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-14-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-edge-armhf"} 1.0
alpinelinux_builds_build_progress_current{builder="build-edge-s390x"} 1.0
alpinelinux_builds_build_progress_current{builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-16-armv7"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-19-x86"} 0.0
alpinelinux_builds_build_progress_current{builder="build-3-18-armv7"} 0.0
# HELP alpinelinux_builds_build_progress_total
# TYPE alpinelinux_builds_build_progress_total gauge
alpinelinux_builds_build_progress_total{builder="build-3-13-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-14-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-19-armhf"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-18-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-edge-armv7"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-19-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-15-s390x"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-18-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-13-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-edge-x86_64"} 7.0
alpinelinux_builds_build_progress_total{builder="build-edge-x86"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-19-x86_64"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-armhf"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-armv7"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-armhf"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-19-armv7"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-17-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-15-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_build_progress_total{builder="build-edge-aarch64"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-18-armhf"} 0.0
alpinelinux_builds_build_progress_total{builder="build-edge-riscv64"} 1.0
alpinelinux_builds_build_progress_total{builder="build-edge-ppc64le"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-16-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-14-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-edge-armhf"} 1.0
alpinelinux_builds_build_progress_total{builder="build-edge-s390x"} 1.0
alpinelinux_builds_build_progress_total{builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-16-armv7"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-19-x86"} 0.0
alpinelinux_builds_build_progress_total{builder="build-3-18-armv7"} 0.0
# HELP alpinelinux_builds_total_progress_current
# TYPE alpinelinux_builds_total_progress_current gauge
alpinelinux_builds_total_progress_current{builder="build-3-13-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-14-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-19-armhf"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-18-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-edge-armv7"} 2950.0
alpinelinux_builds_total_progress_current{builder="build-3-19-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-15-s390x"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-18-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-13-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-edge-x86_64"} 1548.0
alpinelinux_builds_total_progress_current{builder="build-edge-x86"} 2938.0
alpinelinux_builds_total_progress_current{builder="build-3-19-x86_64"} 6096.0
alpinelinux_builds_total_progress_current{builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-armhf"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-armv7"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-armhf"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-19-armv7"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-17-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-15-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_total_progress_current{builder="build-edge-aarch64"} 3191.0
alpinelinux_builds_total_progress_current{builder="build-3-18-armhf"} 0.0
alpinelinux_builds_total_progress_current{builder="build-edge-riscv64"} 2633.0
alpinelinux_builds_total_progress_current{builder="build-edge-ppc64le"} 2878.0
alpinelinux_builds_total_progress_current{builder="build-3-16-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-14-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-edge-armhf"} 5341.0
alpinelinux_builds_total_progress_current{builder="build-edge-s390x"} 2697.0
alpinelinux_builds_total_progress_current{builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-16-armv7"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-19-x86"} 0.0
alpinelinux_builds_total_progress_current{builder="build-3-18-armv7"} 0.0
# HELP alpinelinux_builds_total_progress_total
# TYPE alpinelinux_builds_total_progress_total gauge
alpinelinux_builds_total_progress_total{builder="build-3-13-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-18-aarch64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-aarch64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-14-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-19-armhf"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-aarch64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-18-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-15-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-edge-armv7"} 2951.0
alpinelinux_builds_total_progress_total{builder="build-3-19-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-15-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-15-s390x"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-18-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-14-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-14-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-13-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-edge-x86_64"} 1553.0
alpinelinux_builds_total_progress_total{builder="build-edge-x86"} 2939.0
alpinelinux_builds_total_progress_total{builder="build-3-19-x86_64"} 6097.0
alpinelinux_builds_total_progress_total{builder="build-3-19-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-18-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-armhf"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-armv7"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-armhf"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-19-aarch64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-19-armv7"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-17-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-15-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-13-x86_64"} 0.0
alpinelinux_builds_total_progress_total{builder="build-edge-aarch64"} 3192.0
alpinelinux_builds_total_progress_total{builder="build-3-18-armhf"} 0.0
alpinelinux_builds_total_progress_total{builder="build-edge-riscv64"} 2634.0
alpinelinux_builds_total_progress_total{builder="build-edge-ppc64le"} 2879.0
alpinelinux_builds_total_progress_total{builder="build-3-16-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-13-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-14-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-edge-armhf"} 5342.0
alpinelinux_builds_total_progress_total{builder="build-edge-s390x"} 2698.0
alpinelinux_builds_total_progress_total{builder="build-3-18-ppc64le"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-16-armv7"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-19-x86"} 0.0
alpinelinux_builds_total_progress_total{builder="build-3-18-armv7"} 0.0
$
```
