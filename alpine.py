#!/usr/bin/python

import asyncio
from websockets.sync.client import connect
from enum import Enum
import json


class Type(Enum):
    MISC = 0
    IDLE = 1
    PROGRESS = 2
    ERROR = 3


class Builder:
    Name = None
    Status = "idle"
    Type = Type.IDLE

    CurrentPackage = None
    CurrentPackageVer = None

    BuildProgressCurrent = 0
    BuildProgressTotal = 0
    TotalProgressCurrent = 0
    TotalProgressTotal = 0

    Log = []

    def __init__(self, name):
        self.Name = name

    def update(self, msg):
        self.Log.append(msg)
        self.Type = msg.Type

        if msg.Text != "":
            self.Status = msg.Text

        if "BuildProgress" in msg.Json:
            self.BuildProgressCurrent = int(msg.Json["BuildProgress"]["Current"])
            self.BuildProgressTotal = int(msg.Json["BuildProgress"]["Total"])

        if "TotalProgress" in msg.Json:
            self.TotalProgressCurrent = int(msg.Json["TotalProgress"]["Current"])
            self.TotalProgressTotal = int(msg.Json["TotalProgress"]["Total"])

        if "PackageName" in msg.Json:
            self.CurrentPackage = msg.Json["PackageName"]
            self.CurrentPackageVer = msg.Json["PackageVersion"]

    def __str__(self):
        return self.Name


class Message:
    Type = None
    Builder = None
    Text = None

    Raw = None
    Json = None

    def __init__(self, msg, builders):
        self.Raw = msg
        self.Json = json.loads(self.Raw)
        self.Text = self.Json["Msg"]

        for i in builders:
            if i.Name == self.Json["Builder"]:
                self.Builder = i
        if self.Builder == None:
            self.Builder = Builder(self.Json["Builder"])
            builders.append(self.Builder)

        if "MsgType" in self.Json:
            if self.Json["MsgType"] == "msg":
                self.Type = Type.MISC
            elif self.Json["MsgType"] == "idle":
                self.Type = Type.IDLE
                self.Builder.CurrentPackage = None
                self.Builder.CurrentPackageVer = None
            elif self.Json["MsgType"] == "progress":
                self.Type = Type.PROGRESS
            elif self.Json["MsgType"] == "error":
                self.Type = Type.ERROR

        self.Builder.update(self)

    def __str__(self):
        return self.Raw


class AlpineBuild:
    Url = "ws://build.alpinelinux.org/ws/"
    Ws = None
    Builders = []

    def __init__(self):
        self.Ws = connect(self.Url)

    def sync(self):
        with self.ws as websocket:
            while True:
                Message(websocket.recv(), self.Builders)
