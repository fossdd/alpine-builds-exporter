#!/usr/bin/python

from prometheus_client import start_http_server, Enum, REGISTRY, Gauge, Info
import threading

import alpine


def server():
    start_http_server(8000)


prefix = "alpinelinux_builds_"

status = Info(prefix + "status", "", ["builder"])
status_type = Enum(
    prefix + "status_type", "", ["builder"], states=alpine.Type._member_names_
)
pkg = Info(prefix + "package", "", ["builder"])
build_progress_current = Gauge(prefix + "build_progress_current", "", ["builder"])
build_progress_total = Gauge(prefix + "build_progress_total", "", ["builder"])
total_progress_current = Gauge(prefix + "total_progress_current", "", ["builder"])
total_progress_total = Gauge(prefix + "total_progress_total", "", ["builder"])


def import_data(infos):
    for builder in infos.Builders:
        status.labels(builder.Name).info({"text": builder.Status})
        status_type.labels(builder.Name).state(builder.Type.name)
        build_progress_current.labels(builder.Name).set(builder.BuildProgressCurrent)
        build_progress_total.labels(builder.Name).set(builder.BuildProgressTotal)
        total_progress_current.labels(builder.Name).set(builder.TotalProgressCurrent)
        total_progress_total.labels(builder.Name).set(builder.TotalProgressTotal)

        if builder.CurrentPackage == None:
            pkg.labels(builder.Name).info({})
        else:
            pkg.labels(builder.Name).info(
                {"name": builder.CurrentPackage, "version": builder.CurrentPackageVer}
            )


def main():
    threading.Thread(target=server).start()

    infos = alpine.AlpineBuild()
    with infos.Ws as websocket:
        while True:
            alpine.Message(websocket.recv(), infos.Builders)
            import_data(infos)


if __name__ == "__main__":
    main()
